package com.psp.supermercado;

public class Ejecucion {

	public Ejecucion() {
		
	}
	
	public void metodoSequencial() {
		// Definicion de los clientes
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 1, 1, 3, 3, 5, 7, 9, 11 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 2, 2, 2, 4, 4, 6, 8, 10, 12 });
		
		// Definicion de las cajeras
		Cajera cajera1 = new Cajera("Cajera 1");
		Cajera cajera2 = new Cajera("Cajera 2");
		
		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
	
		cajera1.procesarCompra(cliente1, initialTime);
		cajera2.procesarCompra(cliente2, initialTime);
	}

	public void metodoThread() {
		// Definicion de los clientes
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 1, 1, 3, 3, 5, 7, 9, 11 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 2, 2, 2, 4, 4, 6, 8, 10, 12 });

		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
		
		// Definicion de las cajeras
		CajeraThread cajera1 = new CajeraThread("Cajera 1", cliente1, initialTime);
		CajeraThread cajera2 = new CajeraThread("Cajera 2", cliente2, initialTime);
	
		cajera1.start();
		cajera2.start();
	}
	
	public void metodoRunnable() {
		// Definicion de los clientes
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 1, 1, 3, 3, 5, 7, 9, 11 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 2, 2, 2, 4, 4, 6, 8, 10, 12 });
		
		// Definicion de las cajeras
		Cajera cajera1 = new Cajera("Cajera 1");
		Cajera cajera2 = new Cajera("Cajera 2");
		
		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
		
		Runnable proceso1 = new CajeraRunnable(cliente1, cajera1, initialTime);
		Runnable proceso2 = new CajeraRunnable(cliente2, cajera2, initialTime);
		
		new Thread(proceso1).start();
		new Thread(proceso2).start();
	}
	
}
