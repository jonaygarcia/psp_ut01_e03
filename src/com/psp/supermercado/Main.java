package com.psp.supermercado;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		// Guardar la opcion del men�
		Scanner sn = new Scanner(System.in);
		int opcion;
		
		Ejecucion exec = new Ejecucion();
		
		// Menu de la aplicacion
		System.out.println("1. Ejecuci�n sequencial.");
		System.out.println("2. Ejecuci�n heredando de la clase Thread.");
		System.out.println("3. Ejecuci�n implementando la interfaz Runnable");
		System.out.println("4. Salir");
	            
		System.out.println("Escribe una de las opciones: ");
		opcion = sn.nextInt();
			
		switch(opcion){
			case 1:
				exec.metodoSequencial();
				break;
			case 2:
				exec.metodoThread();
				break;
			case 3:
				exec.metodoRunnable();
				break;
			default:
				System.out.println("Solo n�meros entre 1 y 3");
		}
		
		sn.close();
		
	}

}
