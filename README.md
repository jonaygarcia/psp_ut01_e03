# Multitarea e Hilos en Java (Thread & Runnable)

## Introducción

En esta ejercicio vamos a ver las diferentes maneras de cómo trabajar con __Threads en Java__ (hilos en español).

En esencia la multitarea nos permite ejecutar varios procesos a la vez; es decir, de forma concurrente y por tanto eso nos permite hacer programas que se ejecuten en menor tiempo y sean más eficientes. Evidentemente no podemos ejecutar infinitos procesos de forma concurrente ya que el hardware tiene sus limitaciones, pero raro es a día de hoy los ordenadores que no tengan más de un núcleo por tanto en un procesador con dos núcleos se podrían ejecutar dos procesos a la vez y así nuestro programa utilizaría al máximo los recursos hardware.

Supongamos que tenemos un programa secuencial en el que se han de ejecutar 4 procesos; uno detrás de otro, y estos tardan unos segundos:

![Figura 1](img/01.png)

Si en vez de hacerlo de forma secuencial, lo hiciésemos con 4 hilos, el programa tardaría en ejecutarse solo 20 segundos, es decir el tiempo que tardaría en ejecutarse el proceso más largo. Esto evidentemente sería lo ideal, pero la realidad es que no todo se puede paralelizar y hay que saber el número de procesos en paralelo que podemos lanzar de forma eficiente.

![Figura 1](img/02.png)

En Java para utilizar la multitarea debemos de usar la clase __Thread__ (es decir que la clase que implementemos debe heredar de la clase Thread) y la _clase Thread_ implementa la Interface Runnable. En el siguiente diagrama de clase mostramos la _Interface Runnable_ y la _clase Thread_ con sus principales métodos:

![Figura 1](img/03.png)

## Enunciado del problema

En este ejemplo vamos a simular el proceso de cobro de un supermercado; es decir, unos clientes van con una cesta de productos y una cajera les cobra los productos, pasándolos uno a uno por el escaner de la caja registradora. En este caso la cajera debe de procesar la compra cliente a cliente, es decir que primero le cobra al cliente 1, luego al cliente 2 y así sucesivamente.

Para ello vamos a definir una clase _Cajera_ y una clase _Cliente_ el cual tendrá un _array de enteros_ que representaran los productos que ha comprado y el tiempo que la cajera tardará en pasar el producto por el escaner; es decir, que si tenemos un array con [1,3,5] significará que el cliente ha comprado 3 productos y que la cajera tardara en procesar el producto 1 '1 segundo', el producto 2 '3 segundos' y el producto 3 en '5 segundos', con lo cual tardara en cobrar al cliente toda su compra '9 segundos'.


## Ejecución Secuencial (Un único Thread)


Se define la clase __Cajera.java__:


```java
public class Cajera {
	
	private String nombre;

	// Constructor, getter y setter
	public Cajera (String nombre) {
		this.nombre = nombre;
	}
	
	public void procesarCompra(Cliente cliente, long timeStamp) {

		System.out.println("La cajera " + this.nombre + 
				" COMIENZA A PROCESAR LA COMPRA DEL CLIENTE " + cliente.getNombre() + 
				" EN EL TIEMPO: " + (System.currentTimeMillis() - timeStamp) / 1000	+
				"seg");

		for (int i = 0; i < cliente.getCarroCompra().length; i++) { 
				this.esperarXsegundos(cliente.getCarroCompra()[i]); 
				//System.out.println("Procesado el producto " + (i + 1) +  
				//" ->Tiempo: " + (System.currentTimeMillis() - timeStamp) / 1000 + 
				//"seg");
				System.out.println("Procesado el producto " + (cliente.getCarroCompra()[i]) +
									"(Nº Prod. " + (i + 1) + "; " + cliente.getNombre() + ") " + 
									"-> Tiempo: " + (System.currentTimeMillis() - timeStamp) / 1000 + 
									"seg");
		}

		System.out.println("La cajera " + this.nombre + " HA TERMINADO DE PROCESAR " + 
				cliente.getNombre() + " EN EL TIEMPO: " + 
				(System.currentTimeMillis() - timeStamp) / 1000 + "seg");

	}


	private void esperarXsegundos(int segundos) {
		try {
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}
```

Se define la clase __Cliente.java__:

```java
public class Cliente {
	
	private String nombre;
	private int[] carroCompra;
	
	public Cliente(String nombre, int[] carroCompra) {
		super();
		this.nombre = nombre;
		this.carroCompra = carroCompra;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int[] getCarroCompra() {
		return carroCompra;
	}
	public void setCarroCompra(int[] carroCompra) {
		this.carroCompra = carroCompra;
	}
```

Si ejecutásemos este programa propuesto con dos Clientes y con un solo proceso (que es lo que se suele hacer normalmente), se procesaría primero la compra del Cliente 1 y después la del Cliente 2, con lo cual se tardará el tiempo del Cliente 1 + Cliente 2. 

> __Nota__: Aunque hayamos puesto dos objetos de la clase Cajera (cajera1 y cajera2) no significa que tengamos dos cajeras independientes, lo que estamos diciendo es que dentro del mismo hilo se ejecute primero los métodos de la cajera1 y después los métodos de la cajera2, por tanto a nivel de procesamiento es como si tuviésemos una sola cajera:

Definimos la clase __Main.java__:

```java
public class Main {

	public static void main(String[] args) {
		// Definicion de los clientes
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 1, 1, 3, 3, 5, 7, 9, 11 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 2, 2, 2, 4, 4, 6, 8, 10, 12 });
		
		// Definicion de las cajeras
		Cajera cajera1 = new Cajera("Cajera 1");
		Cajera cajera2 = new Cajera("Cajera 2");
		
		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
	
		cajera1.procesarCompra(cliente1, initialTime);
		cajera2.procesarCompra(cliente2, initialTime);
	}
}
```

Si ejecutamos este código tendremos lo siguiente:

```bash
La cajera Cajera 1 COMIENZA A PROCESAR LA COMPRA DEL CLIENTE Cliente 1 EN EL TIEMPO: 0seg
Procesado el producto 1(Nº Prod. 1; Cliente 1) -> Tiempo: 1seg
Procesado el producto 1(Nº Prod. 2; Cliente 1) -> Tiempo: 2seg
Procesado el producto 3(Nº Prod. 3; Cliente 1) -> Tiempo: 5seg
Procesado el producto 3(Nº Prod. 4; Cliente 1) -> Tiempo: 8seg
Procesado el producto 5(Nº Prod. 5; Cliente 1) -> Tiempo: 13seg
Procesado el producto 7(Nº Prod. 6; Cliente 1) -> Tiempo: 20seg
Procesado el producto 9(Nº Prod. 7; Cliente 1) -> Tiempo: 29seg
Procesado el producto 11(Nº Prod. 8; Cliente 1) -> Tiempo: 40seg
La cajera Cajera 1 HA TERMINADO DE PROCESAR Cliente 1 EN EL TIEMPO: 40seg
La cajera Cajera 2 COMIENZA A PROCESAR LA COMPRA DEL CLIENTE Cliente 2 EN EL TIEMPO: 40seg
Procesado el producto 2(Nº Prod. 1; Cliente 2) -> Tiempo: 42seg
Procesado el producto 2(Nº Prod. 2; Cliente 2) -> Tiempo: 44seg
Procesado el producto 2(Nº Prod. 3; Cliente 2) -> Tiempo: 46seg
Procesado el producto 4(Nº Prod. 4; Cliente 2) -> Tiempo: 50seg
Procesado el producto 4(Nº Prod. 5; Cliente 2) -> Tiempo: 54seg
Procesado el producto 6(Nº Prod. 6; Cliente 2) -> Tiempo: 60seg
Procesado el producto 8(Nº Prod. 7; Cliente 2) -> Tiempo: 68seg
Procesado el producto 10(Nº Prod. 8; Cliente 2) -> Tiempo: 78seg
Procesado el producto 12(Nº Prod. 9; Cliente 2) -> Tiempo: 90seg
La cajera Cajera 2 HA TERMINADO DE PROCESAR Cliente 2 EN EL TIEMPO: 90seg
```

Como vemos se procesa primero la compra del cliente 1 y después la compra del cliente 2 tardando en procesar ambas compras un total de 90 segundos.

## Ejecución heredando de la clase Thread

¿Y si en vez de procesar primero un cliente y después otro, procesásemos los dos a la vez?, ¿Cuanto tardaría el programa en ejecutarse?. Pues bien si en vez de haber sólo una _Cajera_ (es decir un solo hilo), hubiese dos _Cajeras_ (es decir dos hilos o threads) podríamos procesar los dos clientes a la vez y tardar menos tiempo en ejecutarse el programa. Para ello debemos de modificar la clase _Cajera.java_ y hacer que esta clase __herede de la clase Thread__ para heredar y sobre-escribir algunos de sus métodos. 

Definimos la clase __CajeraThread__:

```bash
public class CajeraThread extends Thread {
	
	private String nombre;
	private Cliente cliente;
	private long initialTime;

	public CajeraThread (String nombre, Cliente cliente, long initialTime) {
		this.nombre = nombre;
		this.cliente = cliente;
		this.initialTime = initialTime;
	}

	@Override
	public void run() {

		System.out.println("La cajera " + this.nombre + " COMIENZA A PROCESAR LA COMPRA DEL CLIENTE " 
					+ this.cliente.getNombre() + " EN EL TIEMPO: " 
					+ (System.currentTimeMillis() - this.initialTime) / 1000 
					+ "seg");

		for (int i = 0; i < this.cliente.getCarroCompra().length; i++) { 
			this.esperarXsegundos(cliente.getCarroCompra()[i]); 
			//System.out.println("Procesado el producto " + (i + 1) 
			//+ " del cliente " + this.cliente.getNombre() + "->Tiempo: " 
			//+ (System.currentTimeMillis() - this.initialTime) / 1000 
			//+ "seg");
			System.out.println("Procesado el producto " + (cliente.getCarroCompra()[i]) +
					"(Nº Prod. " + (i + 1) + "; " + cliente.getNombre() + ") " + 
					"-> Tiempo: " + (System.currentTimeMillis() - this.initialTime) / 1000 + 
					"seg");
		}

		System.out.println("La cajera " + this.nombre + " HA TERMINADO DE PROCESAR " 
						+ this.cliente.getNombre() + " EN EL TIEMPO: " 
						+ (System.currentTimeMillis() - this.initialTime) / 1000 
						+ "seg");
	}

	private void esperarXsegundos(int segundos) {
		try {
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}
}
```

Lo primero que vemos y que ya hemos comentado es que la _clase CajeraThread_ __debe de heredar de la clase Thread__.

Otra cosa importante es que hemos sobreescrito el método __run()__ (de ahíi la etiqueta __@Override__) . Este método es imprescindibles sobreescribirlo (ya que es un método que esta en la _clase Runnable_ y la _clase Thread_ Implementa esa Interface) porque en él se va a codificar la funcionalidad que se ha de ejecutar en un hilo; es decir, que lo que se programe en el método "run()" se va a ejecutar de forma secuencial en un hilo. En esta clase _CajeraThread_ se pueden sobreescribir más métodos para que hagan acciones sobre el hilo o thread como por ejemplo, parar el thread, ponerlo en reposos, etc. A continuación vamos a ver como programamos el método Main para que procese a los clientes de forma paralela y ver como se tarda menos en procesar todo.

Definimos la clase __MainThread.java__:


```bash
public class MainThread {

	public static void main(String[] args) {

		// Definicion de los clientes
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 1, 1, 3, 3, 5, 7, 9, 11 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 2, 2, 2, 4, 4, 6, 8, 10, 12 });

		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
		
		// Definicion de las cajeras
		CajeraThread cajera1 = new CajeraThread("Cajera 1", cliente1, initialTime);
		CajeraThread cajera2 = new CajeraThread("Cajera 2", cliente2, initialTime);
	
		cajera1.start();
		cajera2.start();
		
	}
} 
​````

Ahora vamos a ver cual sería el resultado de esta ejecución y vamos a comprobar como efectivamente el programa se ejecuta de forma paralela y tarda solo 50 segundos en terminar su ejecución:


​```bash
La cajera Cajera 1 COMIENZA A PROCESAR LA COMPRA DEL CLIENTE Cliente 1 EN EL TIEMPO: 0seg
La cajera Cajera 2 COMIENZA A PROCESAR LA COMPRA DEL CLIENTE Cliente 2 EN EL TIEMPO: 0seg
Procesado el producto 1(Nº Prod. 1; Cliente 1) -> Tiempo: 1seg
Procesado el producto 2(Nº Prod. 1; Cliente 2) -> Tiempo: 2seg
Procesado el producto 1(Nº Prod. 2; Cliente 1) -> Tiempo: 2seg
Procesado el producto 2(Nº Prod. 2; Cliente 2) -> Tiempo: 4seg
Procesado el producto 3(Nº Prod. 3; Cliente 1) -> Tiempo: 5seg
Procesado el producto 2(Nº Prod. 3; Cliente 2) -> Tiempo: 6seg
Procesado el producto 3(Nº Prod. 4; Cliente 1) -> Tiempo: 8seg
Procesado el producto 4(Nº Prod. 4; Cliente 2) -> Tiempo: 10seg
Procesado el producto 5(Nº Prod. 5; Cliente 1) -> Tiempo: 13seg
Procesado el producto 4(Nº Prod. 5; Cliente 2) -> Tiempo: 14seg
Procesado el producto 7(Nº Prod. 6; Cliente 1) -> Tiempo: 20seg
Procesado el producto 6(Nº Prod. 6; Cliente 2) -> Tiempo: 20seg
Procesado el producto 8(Nº Prod. 7; Cliente 2) -> Tiempo: 28seg
Procesado el producto 9(Nº Prod. 7; Cliente 1) -> Tiempo: 29seg
Procesado el producto 10(Nº Prod. 8; Cliente 2) -> Tiempo: 38seg
Procesado el producto 11(Nº Prod. 8; Cliente 1) -> Tiempo: 40seg
La cajera Cajera 1 HA TERMINADO DE PROCESAR Cliente 1 EN EL TIEMPO: 40seg
Procesado el producto 12(Nº Prod. 9; Cliente 2) -> Tiempo: 50seg
La cajera Cajera 2 HA TERMINADO DE PROCESAR Cliente 2 EN EL TIEMPO: 50seg
```

En este ejemplo vemos como el efecto es como si dos cajeras procesasen la compra de los clientes de forma paralela sin que el resultado de la aplicación sufra ninguna variación en su resultado final, que es el de procesar todas las compras de los clientes de forma independiente. 


## Ejecución Impelentando la Interfaz Runnable

Otra forma de hacer lo mismo pero sin heredar de la _clase Thread_ es __implementar la Interface Runnable__. En este caso no dispondremos ni podremos sobreescribir los métodos de la clase Thread ya que no la vamos a utilizar y sólo vamos a tener que sobreescribir el método __run()__.

En este caso solo será necesario implementar el método __run()__ para que los procesos implementados en ese método se ejecuten en un hilo diferente.

Vamos a ver un ejemplo de cómo utilizando objetos de las clases __Cliente.java__ y __Cajera.java__ podemos implementar la multitarea en la misma clase donde se llama al método Main de la aplicación.

Se define la clase __MainRunnable.java__:

```bash
public class MainRunnable implements Runnable{
	
	private Cliente cliente;
	private Cajera cajera;
	private long initialTime;
	
	public MainRunnable (Cliente cliente, Cajera cajera, long initialTime){
		this.cajera = cajera;
		this.cliente = cliente;
		this.initialTime = initialTime;
	}

	public static void main(String[] args) {
		
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 1, 1, 3, 3, 5, 7, 9, 11 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 2, 2, 2, 4, 4, 6, 8, 10, 12 });
		
		Cajera cajera1 = new Cajera("Cajera 1");
		Cajera cajera2 = new Cajera("Cajera 2");
		
		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
		
		Runnable proceso1 = new MainRunnable(cliente1, cajera1, initialTime);
		Runnable proceso2 = new MainRunnable(cliente2, cajera2, initialTime);
		
		new Thread(proceso1).start();
		new Thread(proceso2).start();

	}

	@Override
	public void run() {
		this.cajera.procesarCompra(this.cliente, this.initialTime);
	}
}
```
En este caso implementamos el método _run()_ dentro de la misma clase donde se encuentra el método Main, y en el llamamos al método de _procesarCompra()_ de la clase Cajera. Dentro del método Main, nos creamos dos objetos de la misma clase en la que estamos ("new MainRunnable") y nos creamos dos objetos de la clase Thread para lanzar los proceso y que se ejecuten estos en paralelo. El resultado de esta ejecución es el mismo que en el caso anterior:

```bash
La cajera Cajera 1 COMIENZA A PROCESAR LA COMPRA DEL CLIENTE Cliente 1 EN EL TIEMPO: 0seg
La cajera Cajera 2 COMIENZA A PROCESAR LA COMPRA DEL CLIENTE Cliente 2 EN EL TIEMPO: 0seg
Procesado el producto 1(Nº Prod. 1; Cliente 1) -> Tiempo: 1seg
Procesado el producto 2(Nº Prod. 1; Cliente 2) -> Tiempo: 2seg
Procesado el producto 1(Nº Prod. 2; Cliente 1) -> Tiempo: 2seg
Procesado el producto 2(Nº Prod. 2; Cliente 2) -> Tiempo: 4seg
Procesado el producto 3(Nº Prod. 3; Cliente 1) -> Tiempo: 5seg
Procesado el producto 2(Nº Prod. 3; Cliente 2) -> Tiempo: 6seg
Procesado el producto 3(Nº Prod. 4; Cliente 1) -> Tiempo: 8seg
Procesado el producto 4(Nº Prod. 4; Cliente 2) -> Tiempo: 10seg
Procesado el producto 5(Nº Prod. 5; Cliente 1) -> Tiempo: 13seg
Procesado el producto 4(Nº Prod. 5; Cliente 2) -> Tiempo: 14seg
Procesado el producto 7(Nº Prod. 6; Cliente 1) -> Tiempo: 20seg
Procesado el producto 6(Nº Prod. 6; Cliente 2) -> Tiempo: 20seg
Procesado el producto 8(Nº Prod. 7; Cliente 2) -> Tiempo: 28seg
Procesado el producto 9(Nº Prod. 7; Cliente 1) -> Tiempo: 29seg
Procesado el producto 10(Nº Prod. 8; Cliente 2) -> Tiempo: 38seg
Procesado el producto 11(Nº Prod. 8; Cliente 1) -> Tiempo: 40seg
La cajera Cajera 1 HA TERMINADO DE PROCESAR Cliente 1 EN EL TIEMPO: 40seg
Procesado el producto 12(Nº Prod. 9; Cliente 2) -> Tiempo: 50seg
La cajera Cajera 2 HA TERMINADO DE PROCESAR Cliente 2 EN EL TIEMPO: 50seg
```

